import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

let baseUrl = "http://192.168.31.166:8080/line-work"

Vue.prototype.url = {
	register: baseUrl + "/tbUser/register",
	login: baseUrl + "/tbUser/login",
	checkin: baseUrl + "/checkin/checkin",
	createFaceModel: baseUrl + "/checkin/createFaceModel",
	validCanCheckIn: baseUrl + "/checkin/validCanCheckIn",
	searchTodayCheckin: baseUrl + "/checkin/searchTodayCheckin",
	isTodayCheckin: baseUrl + "/checkin/isTodayCheckin",
	searchUserSummary: baseUrl + "/tbUser/searchUserSummary",
	searchMonthCheckin: baseUrl + "/checkin/searchMonthCheckin"
}

Vue.prototype.ajax = function(url, method, data, fun) {
	uni.request({
		"url": url,
		"method": method,
		"header": {
			token: uni.getStorageSync('token')
		},
		"data": data,
		success: function(resp) {
			if (resp.statusCode == 401) {
				uni.redirectTo({
					url: '../login/login'
				});
			} else if (resp.statusCode == 200 && resp.data.code == 200) {
				let data = resp.data
				if (data.hasOwnProperty("token")) {
					console.log(resp.data)
					let token = data.token
					uni.setStorageSync("token", token)
				}
				fun(resp)
			} else {
				uni.showToast({
					icon: 'none',
					title: resp.data
				});
			}
		}
	});
}

Vue.prototype.checkPermission = function(perms) {
	let permission = uni.getStorageSync("permission")
	let result = false;
	for (let one of perms) {
		if (permission.indexOf(one) != -1) {
			result = true;
			break;
		}
	}
	return result;
}

const app = new Vue({
	...App
})
app.$mount()
