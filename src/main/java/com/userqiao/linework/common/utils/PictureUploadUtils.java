package com.userqiao.linework.common.utils;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * 文件上传至阿里OSS对象存储
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/2/24 15:04
 */
@Component
public class PictureUploadUtils {
    @Value("${line-work.oss.endpoint}")
    private String endpoint;
    @Value("${line-work.oss.accessKeyId}")
    private String accessKeyId;
    @Value("${line-work.oss.accessKeySecret}")
    private String accessKeySecret;
    @Value("${line-work.oss.bucketName}")
    private String bucketName;

    private static OSSClient ossClient;

    /** 上传文件到阿里云,并生成url
     * @param filedir (key)文件名(不包括后缀)
     * @param in 	文件字节流
     * @param suffix 文件后缀名
     * @return String 生成的文件url
     */
    public String UploadToAliyun(String filedir, InputStream in, String suffix) {
        System.out.println("------------>文件名称为:  " + filedir + "." + suffix);
        ossClient  = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        URL url = null;
        try {
            // 创建上传Object的Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(in.available());
            objectMetadata.setCacheControl("no-cache");//设置Cache-Control请求头，表示用户指定的HTTP请求/回复链的缓存行为:不经过本地缓存
            objectMetadata.setHeader("Pragma", "no-cache");//设置页面不缓存
            objectMetadata.setContentType(getcontentType(suffix));
            objectMetadata.setContentDisposition("inline;filename=" + filedir + "." + suffix);
            // 上传文件
            ossClient.putObject(bucketName, filedir, in, objectMetadata);

            Date expiration = null;//过期时间

            expiration = new Date(System.currentTimeMillis() + 3600L * 1000 * 24 * 365 * 10);
            //}
            // 生成URL
            System.out.println(expiration);
            url = ossClient.generatePresignedUrl(bucketName, filedir, expiration);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return url.toString();
    }

    /**
     * Description: 判断OSS服务文件上传时文件的contentType
     * @param suffix 文件后缀
     * @return String HTTP Content-type
     */
    public static String getcontentType(String suffix) {
        System.out.println("------------>文件格式为:  " + suffix);
        if (suffix.equalsIgnoreCase("bmp")) {
            return "image/bmp";
        } else if (suffix.equalsIgnoreCase("gif")) {
            return "image/gif";
        } else if (suffix.equalsIgnoreCase("jpeg") || suffix.equalsIgnoreCase("jpg")) {
            return "image/jpeg";
        } else if (suffix.equalsIgnoreCase("png")) {
            return "image/png";
        } else if (suffix.equalsIgnoreCase("html")) {
            return "text/html";
        } else if (suffix.equalsIgnoreCase("txt")) {
            return "text/plain";
        } else if (suffix.equalsIgnoreCase("vsd")) {
            return "application/vnd.visio";
        } else if (suffix.equalsIgnoreCase("pptx") || suffix.equalsIgnoreCase("ppt")) {
            return "application/vnd.ms-powerpoint";
        } else if (suffix.equalsIgnoreCase("docx") || suffix.equalsIgnoreCase("doc")) {
            return "application/msword";
        } else if (suffix.equalsIgnoreCase("xml")) {
            return "text/xml";
        } else if (suffix.equalsIgnoreCase("mp3")) {
            return "audio/mp3";
        } else if (suffix.equalsIgnoreCase("amr")) {
            return "audio/amr";
        } else {
            return "text/plain";
        }
    }

    /**删除图片
     * @param key
     */
    public void deletePicture(String key){
        ossClient  = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        ossClient.deleteObject(bucketName, key);
        ossClient.shutdown();
    }
}
