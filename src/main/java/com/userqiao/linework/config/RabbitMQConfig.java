package com.userqiao.linework.config;

import com.rabbitmq.client.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MQ配置类
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/6 12:45
 */
@Configuration
public class RabbitMQConfig {

    @Bean
    public ConnectionFactory getFactory(){
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("userqiao");
        connectionFactory.setPassword("123456");
        return connectionFactory;
    }
}
