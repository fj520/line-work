package com.userqiao.linework.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.userqiao.linework.common.utils.BaseResult;
import com.userqiao.linework.common.utils.PictureUploadUtils;
import com.userqiao.linework.config.shiro.JwtUtil;
import com.userqiao.linework.entity.SystemConstants;
import com.userqiao.linework.entity.vo.CheckinVo;
import com.userqiao.linework.entity.vo.SearchMonthCheckinVo;
import com.userqiao.linework.exception.LineWorkException;
import com.userqiao.linework.service.CheckinService;
import com.userqiao.linework.service.TbUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/2/22 12:57
 */
@RequestMapping("/checkin")
@RestController
@Api("签到模块Web接口")
@Slf4j
public class CheckinController {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private CheckinService checkinService;
    @Autowired
    private PictureUploadUtils pictureUploadUtils;
    @Value("${line-work.image-folder}")
    private String imageFolder;
    @Autowired
    private TbUserService userService;
    @Autowired
    private SystemConstants constants;


    @GetMapping("/validCanCheckIn")
    @ApiOperation("查看用户今天是否可以签到")
    public BaseResult validCanCheckIn(@RequestHeader("token") String token) {
        int userId = jwtUtil.getUserId(token);
        String result = checkinService.validCanCheckIn(userId, DateUtil.today());
        return BaseResult.ok(result);
    }

    @PostMapping("/checkin")
    @ApiOperation("签到")
    public BaseResult checkin(@Valid CheckinVo checkinVo,
                              @RequestParam("photo") MultipartFile file, @RequestHeader("token") String token) {
        if (null == file) {
            return BaseResult.error("没有上传文件");
        }
        int userId = jwtUtil.getUserId(token);
        String fileName = file.getOriginalFilename().toLowerCase();
        String path = imageFolder + "/" + fileName;
        if (!fileName.endsWith(".jpg")) {
            FileUtil.del(path);
            return BaseResult.error("必须提交JPG格式图片");
        } else {
            try {
                file.transferTo(Paths.get(path));
                checkinService.checkin(checkinVo,userId,path);
                return BaseResult.ok("签到成功");
            } catch (IOException e) {
                log.error(e.getMessage());
                throw new LineWorkException("保存图片错误");
            } finally {
                FileUtil.del(path);
            }
        }
    }

    @PostMapping("/createFaceModel")
    @ApiOperation("创建人脸模型")
    public BaseResult createFaceModel(@RequestParam("photo") MultipartFile file, @RequestHeader("token") String token) {
        int userId = jwtUtil.getUserId(token);
        if (file==null) {
            return BaseResult.error("没有上传文件");
        }
        String fileName = file.getOriginalFilename().toLowerCase();
        String path = imageFolder + "/" + fileName;
        if (!fileName.endsWith(".jpg")) {
            return BaseResult.error("必须提交JPG格式图片");
        } else {
            try {
                file.transferTo(Paths.get(path));
                checkinService.createFaceModel(userId, path);
                return BaseResult.ok("人脸建模成功");
            } catch (IOException e) {
                e.printStackTrace();
                throw new LineWorkException("保存图片错误");
            } finally {
                FileUtil.del(path);
            }
        }
    }

    /**
     * 查看用户的当日签到数据
     * @param token
     * @return
     */
    @GetMapping("/searchTodayCheckin")
    @ApiOperation("查询用户当日签到数据")
    public BaseResult searchTodayCheckin(@RequestHeader("token") String token){
        int userId = jwtUtil.getUserId(token);
        Map map = checkinService.searchTodayCheckin(userId);
        map.put("attendanceTime", constants.attendanceTime);
        map.put("closingTime", constants.closingTime);
        long days = checkinService.searchCheckinDays(userId);
        map.put("checkinDays", days);
        //判断日期是否在用户入职之前
        DateTime hiredate = DateUtil.parse(userService.searchUserHiredate(userId));
        DateTime startDate = DateUtil.beginOfWeek(DateUtil.date());
        if (startDate.isBefore(hiredate)) {
            startDate = hiredate;
        }
        DateTime endDate = DateUtil.endOfWeek(DateUtil.date());
        Map param = new HashMap();
        param.put("startDate", startDate.toString());
        param.put("endDate", endDate.toString());
        param.put("userId", userId);
        List<Map> list = checkinService.searchWeekCheckin(param);
        map.put("weekCheckin", list);
        return BaseResult.ok().put("result",map);
    }

    @GetMapping("/isTodayCheckin")
    @ApiOperation("查询当天是否已经签过到了")
    public BaseResult isTodayCheckin(@RequestHeader("token") String token){
        boolean result = false;
        int userId = jwtUtil.getUserId(token);
        Map map = checkinService.searchTodayCheckin(userId);
        if (map.isEmpty()){
            result = true;
        }
        return BaseResult.ok().put("result",result);
    }

    @PostMapping("/searchMonthCheckin")
    @ApiOperation("查询用户某月签到数据")
    public BaseResult searchMonthCheckin(@Valid @RequestBody SearchMonthCheckinVo searchMonthCheckinVo, @RequestHeader("token") String token) {
        int userId = jwtUtil.getUserId(token);
        //查询入职日期
        DateTime hiredate = DateUtil.parse(userService.searchUserHiredate(userId));
        //把月份处理成双数字
        String month = searchMonthCheckinVo.getMonth() < 10 ? "0" + searchMonthCheckinVo.getMonth() : "" + searchMonthCheckinVo.getMonth();
        //某年某月的起始日期
        DateTime startDate = DateUtil.parse(searchMonthCheckinVo.getYear() + "-" + month + "-01");
        //如果查询的月份早于员工入职日期的月份就抛出异常
        if (startDate.isBefore(DateUtil.beginOfMonth(hiredate))) {
            throw new LineWorkException("只能查询考勤之后日期的数据");
        }
        //如果查询月份与入职月份恰好是同月，本月考勤查询开始日期设置成入职日期
        if (startDate.isBefore(hiredate)) {
            startDate = hiredate;
        }
        //某年某月的截止日期
        DateTime endDate = DateUtil.endOfMonth(startDate);
        HashMap param = new HashMap();
        param.put("startDate", startDate.toString());
        param.put("endDate", endDate.toString());
        param.put("userId", userId);
        List<Map> list = checkinService.searchMonthCheckin(param);
        //统计月考勤数据
        int sum_1 = 0, sum_2 = 0, sum_3 = 0;
        for (Map<String, String> map : list) {
            String type = map.get("type");
            String status = map.get("status");
            if ("工作日".equals(type)) {
                if ("正常".equals(status)) {
                    sum_1++;
                } else if ("迟到".equals(status)) {
                    sum_2++;
                } else if ("缺勤".equals(status)) {
                    sum_3++;
                }
            }
        }
        return BaseResult.ok().put("list", list).put("sum_1", sum_1).put("sum_2", sum_2).put("sum_3", sum_3);
    }
}
