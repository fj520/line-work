package com.userqiao.linework.controller;

import com.userqiao.linework.common.utils.BaseResult;
import com.userqiao.linework.config.shiro.JwtUtil;
import com.userqiao.linework.entity.vo.SearchMessageByPageVo;
import com.userqiao.linework.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/** 消息模块的网络接口
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/5 23:34
 */
@RestController
@RequestMapping("/message")
@Api("消息模块的网络接口")
public class MessageController {
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private MessageService messageService;

    @PostMapping("/searchMessageByPage")
    @ApiOperation("获取分页消息列表")
    public BaseResult searchMessageByPage(@Valid @RequestBody SearchMessageByPageVo vo , @RequestHeader("token") String token){
        int userId = jwtUtil.getUserId(token);
        Integer page = vo.getPage();
        Integer length = vo.getLength();
        long start = (page - 1)*length;
        List<Map> list = messageService.searchMessageByPage(userId, start, length);
        return BaseResult.ok().put("result",list);
    }

    @PostMapping("/searchMessageById")
    @ApiOperation("根据ID查询消息")
    public BaseResult searchMessageById(@RequestParam("id") String id){
        Map map = messageService.searchMessageById(id);
        return BaseResult.ok().put("result",map);
    }

    @PostMapping("/updateUnreadMessage")
    @ApiOperation("未读消息更新成已读消息")
    public BaseResult updateUnreadMessage(@NotNull @RequestParam("id") String id) {
        long rows = messageService.updateUnreadMessage(id);
        return BaseResult.ok().put("result", rows == 1 ? true : false);
    }

    @PostMapping("/deleteMessageRefById")
    @ApiOperation("删除消息")
    public BaseResult deleteMessageRefById(@NotNull @RequestParam("id") String id){
        long rows=messageService.deleteMessageRefById(id);
        return BaseResult.ok().put("result", rows == 1 ? true : false);
    }
}
