package com.userqiao.linework.dao;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import com.userqiao.linework.entity.MessageEntity;
import com.userqiao.linework.entity.MessageRefEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Message消息持久层
 *
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/2 23:04
 */
@Repository
public class MessageDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    public String insert(MessageEntity messageEntity) {
        /*
        * 把北京时间转换成格林尼治时间
        * save时mongoose的时间类型数据会自动被转化为格林尼治时间（零时区时间），若让其转，后面读取的时候手动转为东八区会引起一个新的bug
        * 若当前时区不是东八区，这种操作会返回一个错误的时间，因为世界各地转化为格林尼治标准时间的时差不同。
        * 所以这里保存值得时候就先转为格林尼治时间
        * */
        Date sendTime = messageEntity.getSendTime();
        sendTime = DateUtil.offset(sendTime, DateField.HOUR, 8);
        messageEntity.setSendTime(sendTime);
        messageEntity = mongoTemplate.save(messageEntity);
        return messageEntity.get_id();
    }

    /**
     * 根据消息id查询实体信息
     * @param id
     * @return
     */
    public Map searchMessageById(String id){
        Map map = mongoTemplate.findById(id,Map.class,"message");
        Date sendTime = (Date) map.get("sendTime");
        // 从格林尼治时间转为北京东八区时间
        sendTime = DateUtil.date(sendTime).offset(DateField.HOUR, -8);
        map.replace("sendTime",DateUtil.format(sendTime,"yyyy-MM-dd HH:mm"));
        return map;
    }

    /**
     * 分页查询列表
     * @param userId
     * @param start
     * @param length
     * @return
     */
    public List<Map> searchMessageByPage(int userId,long start,int length){
        JSONObject json = new JSONObject();
        json.set("$toString", "$_id");
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.addFields().addField("id").withValue(json).build(),
                Aggregation.lookup("message_ref", "id", "messageId", "ref"),
                Aggregation.match(Criteria.where("ref.receiverId").is(userId)),
                Aggregation.sort(Sort.by(Sort.Direction.DESC, "sendTime")),
                Aggregation.skip(start),
                Aggregation.limit(length)
        );
        AggregationResults<Map> results = mongoTemplate.aggregate(aggregation, "message", Map.class);
        List<Map> list = results.getMappedResults();
        list.forEach(one -> {
            List<MessageRefEntity> refList = (List<MessageRefEntity>) one.get("ref");
            MessageRefEntity entity = refList.get(0);

            boolean readFlag = entity.getReadFlag();
            String refId = entity.get_id();
            one.remove("ref");
            one.put("readFlag", readFlag);
            one.put("refId", refId);
            one.remove("_id");
            //把格林尼治时间转换成北京时间
            Date sendTime = (Date) one.get("sendTime");
            sendTime = DateUtil.offset(sendTime, DateField.HOUR, -8);
            String today = DateUtil.today();
            //如果是今天的消息，只显示发送时间，不需要显示日期
            if (today.equals(DateUtil.date(sendTime).toDateStr())) {
                one.put("sendTime", DateUtil.format(sendTime, "HH:mm"));
            }
            //如果是以往的消息，只显示日期，不显示发送时间
            else {
                one.put("sendTime", DateUtil.format(sendTime, "yyyy/MM/dd"));
            }
        });
        return list;
    }
}
