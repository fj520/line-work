package com.userqiao.linework.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 消息实体
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/2 22:55
 */
@Data
@Document(collation = "message")
public class MessageEntity {

    @Id
    private String _id;
    @Indexed(unique = true)
    private String uuid;
    /**
     * 发送者ID，就是用户ID。如果是系统自动发出，这个ID值是0
     */
    @Indexed
    private Integer senderId;
    /**
     * 发送者的头像URL。在消息页面要显示发送人的头像
     */
    private String senderPhoto = "https://markdown-userqiao.oss-cn-beijing.aliyuncs.com/senderPhoto.png";
    /**
     * 发送者名称，也就是用户姓名。在消息页面要显示发送人的名字
     */
    private String senderName;
    /**
     * 发送时间
     */
    @Indexed
    private Date sendTime;
    /**
     * 消息正文
     */
    private String msg;
}
