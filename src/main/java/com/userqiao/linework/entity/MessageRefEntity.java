package com.userqiao.linework.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 群发消息记录接收人
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/2 23:01
 */
@Document(collection = "message_ref")
@Data
public class MessageRefEntity {
    @Id
    private String _id;
    /**
     * message记录的_id
     */
    @Indexed
    private String messageId;
    /**
     * 接收人ID
     */
    @Indexed
    private Integer receiverId;
    /**
     * 是否已读
     */
    @Indexed
    private Boolean readFlag;
    /**
     * 是否为新接收的消息
     */
    @Indexed
    private Boolean lastFlag;
}
