package com.userqiao.linework.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * 接收小程序提交的签到数据
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/2/24 14:17
 */
@Getter
@Setter
@ApiModel
public class CheckinVo {
    private String address;
    private String country;
    private String province;
    private String city;
    private String district;
}
