package com.userqiao.linework.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 查询消息的查询条件
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/5 22:49
 */
@ApiModel
@Data
public class SearchMessageByPageVo {


    @NotNull
    @Min(1)
    private Integer page;

    @NotNull
    @Range(min = 1,max = 40)
    private Integer length;
}
