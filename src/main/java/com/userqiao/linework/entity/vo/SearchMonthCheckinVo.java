package com.userqiao.linework.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/2/28 13:04
 */
@Data
@ApiModel
public class SearchMonthCheckinVo {
    @NotNull
    @Range(min = 2000, max = 3000)
    private Integer year;
    @NotNull
    @Range(min = 1, max = 12)
    private Integer month;
}
