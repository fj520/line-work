package com.userqiao.linework.service;

import com.userqiao.linework.entity.vo.CheckinVo;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

/**
 * 签到相关业务层
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/2/22 12:49
 */
public interface CheckinService {

    String validCanCheckIn(int userId, String date);

    /**
     * 签到方法
     * @param param
     */
    void checkin(CheckinVo form, int userId, String path);

    /**
     * 创建新员工的人脸模型
     * @param userId
     * @param path
     */
    void createFaceModel(int userId, String path) throws MalformedURLException;

    /**
     * 查看今日是否打卡
     * @param userId
     * @return
     */
    Map searchTodayCheckin(int userId);

    /**
     * 查看总打卡天数
     * @param userId
     * @return
     */
    long searchCheckinDays(int userId);

    /**
     * 查看一周的打卡情况
     * @param param
     * @return
     */
    List<Map> searchWeekCheckin(Map param);

    /**
     * 查看这个月的打卡情况
     * @param param
     * @return
     */
    List<Map> searchMonthCheckin(Map param);
}
