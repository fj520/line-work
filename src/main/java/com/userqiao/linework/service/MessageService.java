package com.userqiao.linework.service;

import com.userqiao.linework.entity.MessageEntity;
import com.userqiao.linework.entity.MessageRefEntity;

import java.util.List;
import java.util.Map;

/**
 * @author：userqiao
 * @email：userqiao@163.com
 * @date：2021/3/5 22:27
 */
public interface MessageService {
    String insertMessage(MessageEntity entity);

    String insertRef(MessageRefEntity entity);

    long searchUnreadCount(int userId);

    long searchLastCount(int userId);

    List<Map> searchMessageByPage(int userId, long start, int length);

    Map searchMessageById(String id);

    long updateUnreadMessage(String id);

    long deleteMessageRefById(String id);

    long deleteUserMessageRef(int userId);
}
